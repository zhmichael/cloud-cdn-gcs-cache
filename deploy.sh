#configuration of project and region, zone
PROJECT_ID=gcp-demo
IMAGE_NAME=cdn-gcs-cache-base-image-20230209
NGINX_PROXY_REGION=asia-southeast1
HEALTHCHECK_NAME=cdn-nginx-healthcheck
PROXY_FIREWALL_RULE_NAME=cdn-nginx-proxy-rule
REDIS_NAME=cdn-file-sync-status
ACCESS_GCS_IAM_ROLE_NAME=nginx_access_gcs_role

#configration for nginx proxy
NGINX_PROXY_MIG_NAME=nginx-proxy-for-cdn 
NGINX_PROXY_TEMPLATE_NAME=$NGINX_PROXY_MIG_NAME
NGINX_PROXY_BASE_INSTANCE_NAME=$NGINX_PROXY_MIG_NAME
NGINX_PROXY_MACHINE_TYPE=n2-standard-8
NGINX_PROXY_MIG_INSTANCE_MIN=1
NGINX_PROXY_MIG_INSTANCE_MAX=1

#configration for cdn origin file synchronizer
SYNC_MIG_NAME=file-sync-for-cdn
SYNC_TEMPLATE_NAME=$SYNC_MIG_NAME
SYNC_BASE_INSTANCE_NAME=$SYNC_MIG_NAME
NGINX_PROXY_MACHINE_TYPENGINX_PROXY_MACHINE_TYPE=e2-standard-4
SYNC_MIG_INSTANCE_MIN=1
SYNC_MIG_INSTANCE_MAX=1

#configuration of LB
LB_NAME=lb-cdn-nginx-proxy
LB_PUBLIC_IP_NAME=public-ip-cdn
LB_BACKEND_NAME=("cdn-backend")

#config file name used by python3 synchronizer, generate automatically and upload to config bucket
ORIGIN_SYNC_PYTHON_CFG=origin_sync_cfg.ini

if [ $# = 0 ]; then
    echo 'no parameter, please use bash deploy.sh create or bash deploy.sh clean'
    exit 0
fi

gcloud config set project $PROJECT_ID

if [ $1 = 'create' ]; then
    echo 'create bucket '
    project_number=$(gcloud projects describe $PROJECT_ID|grep projectNumber| awk -F ' ' '{print $2}'| sed $'s/\'//g')
    # echo 'project number: '$project_number
    config_bucket_name=$project_number'-cdn-config'
    cache_bucket_name=$project_number'-cdn-cache-'$(uuidgen|cut -c 25-36|tr "[:upper:]" "[:lower:]")
    gcloud storage buckets create gs://$config_bucket_name --location $NGINX_PROXY_REGION
    gcloud storage buckets create gs://$cache_bucket_name --location $NGINX_PROXY_REGION --enable-autoclass
    gsutil iam ch allUsers:legacyObjectReader gs://$cache_bucket_name

    source ./global_conf
    if ! [ -f $nginx_conf_gcs ]; then
        echo "file $nginx_conf_gcs not exists"
        exit 1
    fi

    echo 'upload config file to config bucket'
    gcloud storage cp init.sh global_conf $ORIGIN_SYNC_PYTHON_CFG update_nginx_proxy.sh cdn-file-sync.py $nginx_conf_gcs gs://$config_bucket_name/

    echo 'create redis'
    gcloud services enable redis.googleapis.com
    gcloud redis instances create $REDIS_NAME --size=1 --region=$NGINX_PROXY_REGION --tier=STANDARD
    redis_ip=$(gcloud redis instances describe $REDIS_NAME --region=$NGINX_PROXY_REGION|grep host|awk '{print $2}')
    echo '[config]' > $ORIGIN_SYNC_PYTHON_CFG
    echo 'project_id='$PROJECT_ID >> $ORIGIN_SYNC_PYTHON_CFG
    echo 'pubsub_subscription=nginx-access-origin-sub' >> $ORIGIN_SYNC_PYTHON_CFG
    echo 'redis_ip='$redis_ip >> $ORIGIN_SYNC_PYTHON_CFG >> $ORIGIN_SYNC_PYTHON_CFG

    echo 'create pubsub'
    gcloud services enable pubsub.googleapis.com
    gcloud pubsub topics create nginx-access-origin
    gcloud pubsub subscriptions create nginx-access-origin-sub \
        --topic=nginx-access-origin \
        --ack-deadline=60 \
        --expiration-period=3d

    gcloud services enable compute.googleapis.com

    gcloud beta compute health-checks create http $HEALTHCHECK_NAME \
        --port=8081 \
        --request-path=/index.html \
        --proxy-header=NONE \
        --no-enable-logging \
        --check-interval=5 \
        --timeout=5 \
        --unhealthy-threshold=3 \
        --healthy-threshold=1

    echo 'create logging sink to pubsub'
    gcloud logging sinks create nginx-access-origin \
        pubsub.googleapis.com/projects/$PROJECT_ID/topics/nginx-access-origin \
        --description='sink the nginx proxy access origin log to pubsub' \
        --log-filter='resource.type="gce_instance" AND logName="projects/'$PROJECT_ID'/logs/nginx-access" AND jsonPayload.httpRequest.isFromGcs=0 AND httpRequest.status<300' 
    logging_agent_sa=$(gcloud logging sinks describe nginx-access-origin|grep writerIdentity| awk '{print $2}'|awk -F ':' '{print $2}')
    gcloud pubsub topics add-iam-policy-binding nginx-access-origin \
        --member="serviceAccount:$logging_agent_sa" \
        --role='roles/pubsub.publisher'

    echo 'create nginx proxy MIG and cdn file sync MIG'
    gcloud compute firewall-rules create $PROXY_FIREWALL_RULE_NAME \
        --direction=INGRESS \
        --priority=1000 \
        --network=default \
        --action=ALLOW \
        --rules=tcp:80,tcp:8080,tcp:8081 \
        --source-ranges=0.0.0.0/0 \
        --target-tags=http-server,https-server

    #create nginx proxy instance group
    gcloud compute instance-templates create $NGINX_PROXY_TEMPLATE_NAME \
        --machine-type=$NGINX_PROXY_MACHINE_TYPE \
        --network-interface=network=default,network-tier=PREMIUM,address='' \
        --metadata=cdn-config-bucket=$config_bucket_name,instance-role=nginx-proxy \
        --maintenance-policy=MIGRATE \
        --provisioning-model=STANDARD \
        --scopes=https://www.googleapis.com/auth/cloud-platform \
        --tags=http-server,https-server \
        --create-disk=auto-delete=yes,boot=yes,device-name=$NGINX_PROXY_TEMPLATE_NAME,image=projects/$PROJECT_ID/global/images/$IMAGE_NAME,mode=rw,size=100,type=pd-balanced

    gcloud beta compute instance-groups managed create $NGINX_PROXY_MIG_NAME \
        --base-instance-name=$NGINX_PROXY_BASE_INSTANCE_NAME \
        --size=1 \
        --template=$NGINX_PROXY_TEMPLATE_NAME \
        --region=$NGINX_PROXY_REGION \
        --health-check=$HEALTHCHECK_NAME \
        --initial-delay=120

    gcloud compute instance-groups set-named-ports $NGINX_PROXY_MIG_NAME \
        --named-ports http:80 \
        --region=$NGINX_PROXY_REGION

    gcloud beta compute instance-groups managed set-autoscaling $NGINX_PROXY_MIG_NAME \
        --region=$NGINX_PROXY_REGION \
        --cool-down-period=60 \
        --max-num-replicas=$NGINX_PROXY_MIG_INSTANCE_MAX \
        --min-num-replicas=$NGINX_PROXY_MIG_INSTANCE_MIN \
        --mode=on \
        --target-cpu-utilization=0.6

    #create cdn origin file synchronizer instance group
    gcloud compute instance-templates create $SYNC_TEMPLATE_NAME \
        --machine-type=$SYNC_MACHINE_TYPE \
        --network-interface=network=default,network-tier=PREMIUM,address='' \
        --metadata=cdn-config-bucket=$config_bucket_name,instance-role=cdn-origin-file-sync \
        --maintenance-policy=MIGRATE \
        --provisioning-model=STANDARD \
        --scopes=https://www.googleapis.com/auth/cloud-platform \
        --tags=http-server,https-server \
        --create-disk=auto-delete=yes,boot=yes,device-name=$SYNC_TEMPLATE_NAME,image=projects/$PROJECT_ID/global/images/$IMAGE_NAME,mode=rw,size=100,type=pd-balanced

    gcloud beta compute instance-groups managed create $SYNC_MIG_NAME \
        --base-instance-name=$SYNC_BASE_INSTANCE_NAME \
        --size=1 \
        --template=$SYNC_TEMPLATE_NAME \
        --region=$NGINX_PROXY_REGION \
        --health-check=$HEALTHCHECK_NAME \
        --initial-delay=120

    gcloud compute instance-groups set-named-ports $SYNC_MIG_NAME \
        --named-ports http:80 \
        --region=$NGINX_PROXY_REGION

    gcloud beta compute instance-groups managed set-autoscaling $SYNC_MIG_NAME \
        --region=$NGINX_PROXY_REGION \
        --cool-down-period=60 \
        --max-num-replicas=$SYNC_MIG_INSTANCE_MAX \
        --min-num-replicas=$SYNC_MIG_INSTANCE_MIN \
        --mode=on \
        --target-cpu-utilization=0.6

    create load balancer
    gcloud compute addresses create $LB_PUBLIC_IP_NAME \
        --ip-version=IPV4 \
        --network-tier=PREMIUM \
        --global

    for backend_name in ${LB_BACKEND_NAME[@]}; do
        gcloud compute backend-services create $backend_name \
            --load-balancing-scheme=EXTERNAL \
            --protocol=HTTP \
            --port-name=http \
            --health-checks=$HEALTHCHECK_NAME \
            --enable-cdn \
            --cache-mode=FORCE_CACHE_ALL \
            --enable-logging \
            --logging-sample-rate=0.01 \
            --global

        gcloud compute backend-services add-backend $backend_name \
            --instance-group=$NGINX_PROXY_MIG_NAME \
            --instance-group-region=$NGINX_PROXY_REGION \
            --balancing-mode=UTILIZATION \
            --max-utilization=0.8 \
            --global
    done

    gcloud compute url-maps create $LB_NAME \
        --default-service ${LB_BACKEND_NAME[0]}

    gcloud compute target-http-proxies create "$LB_NAME-target-proxy" \
        --url-map=$LB_NAME

    gcloud compute forwarding-rules create "$LB_NAME-forwarding-rule" \
        --load-balancing-scheme=EXTERNAL \
        --address=$LB_PUBLIC_IP_NAME \
        --global \
        --target-http-proxy="$LB_NAME-target-proxy" \
        --ports=80

    # gcloud compute addresses describe $LB_PUBLIC_IP_NAME --global | grep address:
    echo 'config bucket name: '$config_bucket_name
    echo 'cache bucket name: '$cache_bucket_name
    echo "============================================================================================="

elif [ $1 = 'clean' ]; then
    gcloud compute forwarding-rules delete "$LB_NAME-forwarding-rule" --global --quiet
    gcloud compute target-http-proxies delete "$LB_NAME-target-proxy" --global --quiet
    gcloud compute url-maps delete $LB_NAME --global --quiet
    for backend_name in ${LB_BACKEND_NAME[@]}; do
        gcloud compute backend-services delete $backend_name --global --quiet
    done
    gcloud compute addresses delete $LB_PUBLIC_IP_NAME --global --quiet

    gcloud redis instances delete $REDIS_NAME --region=$NGINX_PROXY_REGION --quiet
    gcloud pubsub subscriptions delete nginx-access-origin-sub --quiet
    gcloud pubsub topics delete nginx-access-origin --quiet
    gcloud logging sinks delete nginx-access-origin --quiet

    gcloud compute instance-groups managed delete $NGINX_PROXY_MIG_NAME --region=$NGINX_PROXY_REGION --quiet
    gcloud compute instance-templates delete $NGINX_PROXY_TEMPLATE_NAME --quiet
    gcloud compute instance-groups managed delete $SYNC_MIG_NAME --region=$NGINX_PROXY_REGION --quiet
    gcloud compute instance-templates delete $SYNC_TEMPLATE_NAME --quiet
    gcloud beta compute health-checks delete $HEALTHCHECK_NAME --quiet
    gcloud compute firewall-rules delete $PROXY_FIREWALL_RULE_NAME --quiet
else
    echo 'unkown parameter, please use bash deploy.sh create or bash deploy.sh clean'
fi
