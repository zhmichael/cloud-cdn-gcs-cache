import os
import fcntl
import psutil
import time
import json
import logging
import hashlib
import redis
import sys
import signal
import multiprocessing
import uuid
from google.cloud import pubsub_v1
from google.cloud import storage
from google.api_core import retry
from multiprocessing import Pool
from logging.handlers import RotatingFileHandler
from datetime import datetime
import configparser

# REDIS_IP = '10.48.125.3'
REDIS_PORT = 6379

# PROJECT_ID = "cdn-for-xhs-test"
# pubsub_sub = "nginx-access-origin-sub"
CFG_FILE_PATH = "/mnt/gcs/origin_sync_cfg.ini"
# DOMAIN_NAME = "https://gg-source.loklok.tv"
RETRY_NUM = 10
REDIS_KEY_EXPIRATION_TIME = 600
NUM_MESSAGES = 1
willTerminate=False
LOG_FOLDER="/tmp/cdn-sync-log"
TEMP_FOLDER='/tmp/cdn-download-temp'

TASK_STATUS_SUCCESS='success'
TASK_STATUS_DOWNLOAD_FAIL = 'download_origin_fail'
TASK_STATUS_UPLOAD_FAIL = 'upload_gcs_fail'

UA_NAME = 'Google-CDN-Mirror'

class MyFilter(logging.Filter):
    def __init__(self, name):
        self.filename = name

    def filter(self, record):
        if record.filename == self.filename:
            return True
        return False

def signal_handler(signal,frame):
    logger = logging.getLogger(str(os.getpid()))
    global willTerminate
    logger.info('You pressed Ctrl+C!')
    willTerminate=True

def blob_exists(bucket_name, filename):
   client = storage.Client()
   bucket = client.get_bucket(bucket_name)
   blob = bucket.blob(filename)
   return blob.exists()

def isNeedFileDownload(json_msg, pool):
    logger = logging.getLogger(str(os.getpid()))
    isNeedDownload = False
    url = json_msg["jsonPayload"]["httpRequest"]["host"]+json_msg["jsonPayload"]["httpRequest"]["uri"]
    hash_md5 = hashlib.md5(url.encode("utf8"))
    redis_key = hash_md5.hexdigest()
    logger.info('url md5 to redis key: '+redis_key)
    redis_value = None
    try:
        r = redis.Redis(connection_pool=pool, decode_responses=True)
        redis_value = r.get(redis_key)
    except Exception as e:
            logger.warning('fail to get key')
            logger.warning(e)
            redis_value=None
    if redis_value == None:
        logger.info('cannot find the key from redis, check gcs')
        gcs_filename = "{origin_name}{uri}".format(
            origin_name=json_msg["jsonPayload"]["originName"],
            uri=json_msg["jsonPayload"]["httpRequest"]["uri"])
        if blob_exists(json_msg["jsonPayload"]["bucketName"], gcs_filename)==True:
            logger.info('find the file in gcs, do not need to download it')
            return False
        else:
            logger.info('not find file in gcs, need download it')
            isNeedDownload = True
    else:
        logger.info('find the key in redis, other process/thread is downloading this file')
        return False

#1:set, 0:delete
def setKeyinRedis(json_msg, pool, status):
    logger = logging.getLogger(str(os.getpid()))
    url = json_msg["jsonPayload"]["httpRequest"]["host"]+json_msg["jsonPayload"]["httpRequest"]["uri"]
    hash_md5 = hashlib.md5(url.encode("utf8"))
    redis_key = hash_md5.hexdigest()
    try:
        r = redis.Redis(connection_pool=pool, decode_responses=True)
        if 1 == status:
            logger.debug('set {key}:{value} in redis'.format(key=redis_key,value=status))
            return r.set(redis_key, 1, ex=REDIS_KEY_EXPIRATION_TIME, nx=True)
        else:
            logger.debug('delete {key} in redis'.format(key=redis_key))
            return r.delete(redis_key)
    except Exception as e:
            logger.warning('fail to set key or delete key')
            logger.warning(e)
            return True

def stackdriver_logging(stackdriver_logger, timestamp, insertId, url, status, size, download_time, download_retry_num, upload_time, upload_retry_time, fail_reason):
    content={}
    content['timestamp'] = timestamp
    content['insertId'] = insertId
    content['url'] = url
    content['status'] = status
    content['size'] = size
    content['download_time'] = download_time
    content['download_retry_num'] = download_retry_num
    content['upload_time'] = upload_time
    content['upload_retry_time'] = upload_retry_time
    content['fail_reason'] = fail_reason
    if TASK_STATUS_SUCCESS != status: 
        stackdriver_logger.log_struct(content, severity="ERROR")
    else:
        stackdriver_logger.log_struct(content)

def callback(message, redis_ip, stackdriver_logger):
    logger = logging.getLogger(str(os.getpid()))
    data = message.data.decode("utf-8")
    json_msg = json.loads(data)
    now = datetime.now()
    timestamp = str(datetime.fromtimestamp(datetime.timestamp(now)))

    # logger.info(json_msg)

    origin_host=json_msg["jsonPayload"]["originHost"]
    # logger.info(f'origin host: {origin_host}')
    host_name = json_msg["jsonPayload"]["httpRequest"]["host"]
    # logger.info(f'host name: {host_name}')
    full_url = json_msg["httpRequest"]["requestUrl"].replace(host_name, origin_host)
    # full_url = DOMAIN_NAME+json_msg["jsonPayload"]["httpRequest"]["uri"]
    logger.info(f'new download task: {full_url}')
    #only for test
    # json_msg["jsonPayload"]["httpRequest"]["uri"] = '/' + str(uuid.uuid4()) + json_msg["jsonPayload"]["httpRequest"]["uri"] 
    logger.debug(json_msg["jsonPayload"]["httpRequest"]["uri"])

    pool = redis.ConnectionPool(host=redis_ip, port=REDIS_PORT, db=0, socket_timeout=2, socket_connect_timeout=2)

    if False == isNeedFileDownload(json_msg, pool):
        logger.info('find the file in gcs or other process is downloading it, do not download it')
        return False

    result = setKeyinRedis(json_msg, pool, 1)
    if True == result:
        logger.info('set key in redis return True, begin to download the file')
    else:
        logger.info('set key in redis return None, another process has started the download')
        return False

    file_path=f'{TEMP_FOLDER}/{str(uuid.uuid4())}'
    cmd = f'axel -n 8 -o {file_path} --header=\"User-Agent: {UA_NAME}\" \"{full_url}\" --quiet'
    logger.info(cmd)

    wget_retry_num = 0
    isSuccess = False
    download_time = 0.0;
    while wget_retry_num < RETRY_NUM:
        start = time.time()
        result = os.system(cmd)
        download_time = format(time.time() - start , '.3f')
        if result == 0:
            logger.info(f'wget successfully, use time {download_time} seconds')
            isSuccess = True
            break
        else:
            logger.error(f'failed to wget, will retry for {wget_retry_num} times')
        # time.sleep(pow(i, 2))
        time.sleep(2)
        wget_retry_num = wget_retry_num+1

    if isSuccess == False:
        logger.error('failed to wget after retry')
        setKeyinRedis(json_msg, pool, 0)
        stackdriver_logging(stackdriver_logger=stackdriver_logger, 
            timestamp=timestamp, 
            insertId=json_msg["insertId"], 
            url=full_url, 
            status=TASK_STATUS_DOWNLOAD_FAIL, 
            size=-1, 
            download_time=-1, 
            download_retry_num=wget_retry_num, 
            upload_time=-1, 
            upload_retry_time=-1, 
            fail_reason='fail')
        return False

    if False == os.path.exists(file_path):
        logger.error(f'file {file_path} not exists')
        stackdriver_logging(stackdriver_logger=stackdriver_logger, 
            timestamp=timestamp, 
            insertId=json_msg["insertId"], 
            url=full_url, 
            status=TASK_STATUS_DOWNLOAD_FAIL, 
            size=-1, 
            download_time=-1, 
            download_retry_num=wget_retry_num, 
            upload_time=-1, 
            upload_retry_time=-1, 
            fail_reason='fail')
        return False

    file_size = os.path.getsize(file_path)

    gcs_path = "gs://{bucket_name}/{origin_name}{uri}".format(bucket_name=json_msg["jsonPayload"]["bucketName"], 
        origin_name=json_msg["jsonPayload"]["originName"],
        uri=json_msg["jsonPayload"]["httpRequest"]["uri"])
    # cmd = f'gsutil -m cp {file_path} {gcs_path} >/dev/null 2>&1'
    cmd = f'gcloud storage cp {file_path} {gcs_path} --cache-control=\"public, max-age=3600\" >/dev/null 2>&1'
    logger.info(cmd)

    upload_retry_num = 0
    isSuccess = False
    upload_time = 0.0
    while upload_retry_num < RETRY_NUM:
        start = time.time()
        result = os.system(cmd)
        upload_time = format(time.time() - start , '.3f')
        if result == 0:
            logger.info(f'upload file to gcs successfully, use time {upload_time} seconds')
            isSuccess = True
            break
        else:
            logger.error(f'failed upload the file to gcs, will retry for {upload_retry_num} times')
        # time.sleep(pow(i, 2))
        time.sleep(2)
        upload_retry_num=upload_retry_num+1

    os.system(f'rm -rf {file_path}')

    if isSuccess == False:
        logger.error('failed to gcloud storage after retry')
        setKeyinRedis(json_msg, pool, 0)
        stackdriver_logging(stackdriver_logger=stackdriver_logger, 
            timestamp=timestamp, 
            insertId=json_msg["insertId"], 
            url=full_url, 
            status=TASK_STATUS_UPLOAD_FAIL, 
            size=file_size, 
            download_time=download_time, 
            download_retry_num=wget_retry_num, 
            upload_time=-1, 
            upload_retry_time=upload_retry_num, 
            fail_reason='fail')
        return False

    setKeyinRedis(json_msg, pool, 0)
    stackdriver_logging(stackdriver_logger=stackdriver_logger, 
        timestamp=timestamp, 
        insertId=json_msg["insertId"], 
        url=full_url, 
        status=TASK_STATUS_SUCCESS, 
        size=file_size, 
        download_time=download_time, 
        download_retry_num=wget_retry_num, 
        upload_time=upload_time, 
        upload_retry_time=upload_retry_num, 
        fail_reason='successful')
    logger.info('download from origin and upload to gcs successfully')
    return True

def __init__(parent=False):
    signal.signal(signal.SIGINT,signal_handler)
    signal.signal(signal.SIGTERM,signal_handler)

    if not os.path.exists(LOG_FOLDER):
        os.makedirs(LOG_FOLDER)

    if not os.path.exists(TEMP_FOLDER):
        os.makedirs(TEMP_FOLDER)
    
    logger = logging.getLogger(str(os.getpid()))
    logger.setLevel(logging.INFO)
    # add a rotating handler
    if parent == False:
        log_file_name="{folder}/{name}.log".format(folder= LOG_FOLDER, name=str(os.getpid()))
    else:
        log_file_name="{folder}/{name}.log".format(folder= LOG_FOLDER, name='parent')
    handler = RotatingFileHandler(log_file_name, maxBytes=1024*1024*16, backupCount=5)
    formatter = logging.Formatter('%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)
    return logger
    

#process callback
def long_time_task(name,parent):
    logger = __init__(False)
    logger.info("Run task %s (%s)..." % (name, os.getpid()))
    subscriber = pubsub_v1.SubscriberClient()

    logger.info("load config")
    config = configparser.ConfigParser()
    config.read(CFG_FILE_PATH)
    config.sections()
    logger.info("project id: %s, pubsub_subscription: %s, redis ip: %s" % (config['config']['project_id'], 
        config['config']['pubsub_subscription'], 
        config['config']['redis_ip']))

    subscription_path = subscriber.subscription_path(config['config']['project_id'], config['config']['pubsub_subscription'])

    from google.cloud import logging
    stackdriver_logger = logging.Client().logger('nginx-download')

    # close the underlying gRPC channel when done.
    with subscriber:
        # The subscriber pulls a specific number of messages. The actual
        # number of messages pulled may be smaller than max_messages.
        while willTerminate==False:
            # logger.debug(willTerminate)
            # logger.debug('begin to pull msg')
            #if parent quit, sub process quit also
            p = psutil.Process(parent)
            if p.status() not in  [psutil.STATUS_RUNNING,psutil.STATUS_SLEEPING]:
                logger.info(f"parent process {p} quit, {os.getpid()} will quit")
                sys.exit(0)
            response = subscriber.pull(
                request={"subscription": subscription_path,
                        "max_messages": NUM_MESSAGES},
                retry=retry.Retry(deadline=300),
            )

            if len(response.received_messages) > 0:
                ack_ids = []
                messages = []
                for received_message in response.received_messages:
                    # logger.info(f"Received: {received_message.message.data}.")
                    ack_ids.append(received_message.ack_id)
                    messages.append(received_message.message)
                # Acknowledges the received messages so they will not be sent again.
                subscriber.acknowledge(
                    request={"subscription": subscription_path, "ack_ids": ack_ids}
                )
                for received_message in messages:
                    try:
                        callback(received_message, config['config']['redis_ip'], stackdriver_logger)
                    except Exception as e:
                        logger.warning('fail to execute callback function')
                        logger.warning(e)
                logger.info(
                    f"Received and acknowledged {len(response.received_messages)} messages from {subscription_path}."
                )

def acquire(lock_file):
    open_mode = os.O_RDWR | os.O_CREAT | os.O_TRUNC
    fd = os.open(lock_file, open_mode)

    pid = os.getpid()
    lock_file_fd = None
    
    timeout = 3.0
    start_time = current_time = time.time()
    while current_time < start_time + timeout:
        try:
            fcntl.flock(fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except (IOError, OSError):
            pass
        else:
            lock_file_fd = fd
            break
        print(f'  {pid} waiting for lock')
        time.sleep(1.0)
        current_time = time.time()
    if lock_file_fd is None:
        os.close(fd)
    return lock_file_fd

def release(lock_file_fd):
    fcntl.flock(lock_file_fd, fcntl.LOCK_UN)
    os.close(lock_file_fd)
    return None

if __name__ == "__main__":
    logger = __init__(True)

    pid = os.getpid()
    logger.info(f'{pid} is waiting for lock')
    
    fd = acquire('myfile.lock')

    if fd is None:
        logger.info(f'ERROR: {pid} lock NOT acquired')
        sys.exit(0)
    
    logger.info(f"{pid} lock acquired...")
    
    logger.info("Parent process %s." % os.getpid())
    process_quantity = multiprocessing.cpu_count()
    logger.info('cpu quantity: {cpu_quantity}'.format(cpu_quantity=process_quantity))
    p = Pool(process_quantity)
    for i in range(process_quantity):
        p.apply_async(long_time_task, args=(i, os.getpid()))
    logger.info("Waiting for all subprocess done...")
    p.close()
    p.join()
    logger.info("All subprocess done.")