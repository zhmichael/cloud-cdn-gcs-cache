## TL;DR
This is a automatic deployement solution of Google Cloud CDN layer 2 cache. In this solution, will use the GCS as layer 2 cache, automatically sync the file from the orgin. 
This solution is similar to Aliyun OSS mirror: 
https://www.alibabacloud.com/help/en/oss/user-guide/mirroring-based-back-to-origin

<img src="image/oss-mirror.jpg" width=75%>

## Architecture
<img src="image/arch.jpg" width=75%>

## Automatic Deployement
1. Contact Michael to copy the Nginx image to your project
<img src="image/copy-image.jpg" width=50%>

2. Build the nginx configuration file and test them in your test environment.
<img src="image/nginx_conf.jpg" width=50%>

3. Config the global_conf file, and set the file name in step 2
<img src="image/global_conf.jpg" width=50%>

5. Modify the paramneter in deploy.sh according to your deployement requirement.
<img src="image/deploy_file.jpg" width=50%>

| Parameter| Description |
| --- | ------ |
|     |        |
|     |        |
**PROJECT_ID** | You project id
**IMAGE_NAME**| Image name you copied in step 1.
**NGINX_PROXY_MACHINE_TYPE**| Nginx proxy instance type
**NGINX_PROXY_MACHINE_TYPE**| File sync instance type
**NGINX_PROXY_MIG_INSTANCE_MIN**| Min number of nginx proxy instance
**NGINX_PROXY_MIG_INSTANCE_MAX**| Max number of nginx proxy instance
**SYNC_MIG_INSTANCE_MIN**| Min number of file sync instance
**SYNC_MIG_INSTANCE_MAX**| Max number of file sync instance

6. Run "bash deploy.sh create" and wait for several minuts until the deployment is finished.
<img src="image/deploy1.jpg" width=75%>
<img src="image/deploy2.jpg" width=75%>

7. You will see these resources which be created
- configuration bucket
- cache bucket with Autoclass
- Nginx proxy MIG
- File sync MIG with Auto Scaling
- PubSub topic named "nginx-access-origin" and subscription "nginx-access-origin-sub" to receive the 403 error log in Nginx access
- Load balancing with CDN enabled, with the backend of Nginx proxy MIG


## Nginx Conf File Change Management
- The configuration GCS bucket is mounted to /mnt/gcs folder in each VM by using GCS Fuse
- The conf file nginx_xxx.conf in GCS bucket is soft link to /etc/nginx.conf
- The cron job will run every minute to check if /mnt/gcs/global_conf is changed. If changed, will reload the new nginx conf file 
- Before the reload, the cron job will use nginx -t to check if the conf file is valid
- When the new conf file is reloaded or failed, the result will be displayed in cloud logging.

Run "bash deploy.sh clean" can clean the entire environment
<img src="image/clean.jpg" width=75%>
