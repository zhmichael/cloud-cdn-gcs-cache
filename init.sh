#!/bin/bash

update_cron() {
    #cronjob has not the environment variable
    ln -sf /snap/bin/gcloud /usr/bin/gcloud
    mv /etc/crontab /etc/crontab.bak
    sed -e '/update_nginx_proxy.sh/d' /etc/crontab.bak >/etc/crontab
    echo "* * * * * root python3 /mnt/gcs/cdn-file-sync.py" >>/etc/crontab
    service cron restart
}

nginx_type=$(curl -s 'http://metadata.google.internal/computeMetadata/v1/instance/attributes/instance-role' -H 'Metadata-Flavor: Google')
nginx_conf_gcs=nginx_gcs.conf
source /mnt/gcs/global_conf

if [ $nginx_type = 'nginx-proxy' ]; then
    ln -sf /mnt/gcs/$nginx_conf_gcs /etc/nginx/nginx.conf
    echo $nginx_conf_gcs >/etc/nginx/current_nginx_conf
    service nginx restart
else
    echo 'instance role: cdn-origin-file-sync'
    ln -sf /mnt/gcs/$nginx_conf_gcs /etc/nginx/nginx.conf
    echo $nginx_conf_gcs >/etc/nginx/current_nginx_conf
    service nginx restart
    service google-fluentd stop
    update_cron
fi
