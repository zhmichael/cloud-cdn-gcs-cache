user root;
worker_processes auto;
worker_rlimit_nofile 1048576;

thread_pool default threads=32 max_queue=65536;

events {
    worker_connections 81920;
    multi_accept off;
    accept_mutex off;
}

http {
    include mime.types;
    default_type application/octet-stream;
    server_names_hash_bucket_size 96;

    log_format json_combined
        escape=json
        '{'
        '"time":"$msec",'
        '"bucketName":"$bucket_name",'
        '"originName":"$origin_name",'
        '"originHost":"$proxy_x_origin",'
        '"httpRequest":{'
        '"host":"$host",'
        '"requestMethod":"$request_method",'
        '"requestUrl":"$scheme://$host$request_uri",'
        '"uri":"$uri",'
        '"requestSize":$request_length,'
        '"status":"$status",'
        '"responseSize":$bytes_sent,'
        '"userAgent":"$http_user_agent",'
        '"remoteIp":"$remote_addr",'
        '"serverIp":"$server_addr",'
        '"referer":"$http_referer",'
        '"latency":"${request_time}",'
        '"protocol":"$server_protocol",'
        '"range":"$http_range",'
        '"region":"$http_x_region",'
        '"upstreamAddr":"$upstream_addr",'
        '"upstreamConnectTime":"$upstream_connect_time",'
        '"upstreamHeaderTime":"$upstream_header_time",'
        '"upstreamResponseTime":"$upstream_response_time",'
        '"isFromGcs":"$is_from_gcs",'
        '"cacheStatus": "$upstream_cache_status"'
        '}'
        '}';

    error_log /var/log/nginx/error.log error;
    access_log /var/log/nginx/access.log json_combined;

    sendfile on;
    sendfile_max_chunk 256k;
    tcp_nopush on;

    etag off;
    server_tokens off;

    keepalive_timeout 620s;
    keepalive_requests 3600;
    proxy_read_timeout 30s;
    proxy_connect_timeout 30s;
    proxy_send_timeout 30s;
    send_timeout 30s;

    resolver 169.254.169.254 8.8.8.8 valid=60 ipv6=off;

    upstream upstream_gcs {
        server storage.googleapis.com:443;
        keepalive 600;
    }

    ###################################################################
    # origin1 
    server {
        listen 80 reuseport;
        server_name *.bilivideo.com;
        resolver 8.8.8.8;
        set $bucket_name 61611906141-cdn-cache-c31a6b76cfef;
        set $upstream_hostname storage.googleapis.com;
        set $origin_name origin1;
        set $sign_url 1;
        set $ignore_param 1;

        location / {
            # access_log /var/log/nginx/access_80.log json_combined;
            set $is_from_gcs 1;

            #forbid the requst without signature
            if ($request_uri !~ "(.*)[&|\?]Signature=.*") {
                #If return 403, wget the file will trigger @handle_gcs_404 
                set $is_from_gcs 0;
                return 400;
            }

            proxy_set_header Connection '';
            proxy_http_version 1.1;
            proxy_set_header Host storage.googleapis.com;
            proxy_pass https://storage.googleapis.com/$bucket_name/$origin_name$request_uri;
            add_header From-GCP-GCS 'Yes';
            proxy_intercept_errors on;
            error_page 404 403 = @handle_gcs_404;
        }

        location @handle_gcs_404 {
            set $is_from_gcs 0;
            set $upstream_hostname $host;
            add_header GCP-GCS-404 'Yes';
            # add_header GCP-GCS-PATH $upstream_endpoint/$bucket_name/$origin_name$uri;
            set $proxy_x_origin upos-sz-originbstar.bilivideo.com;
            proxy_set_header Host $proxy_x_origin;
            proxy_pass https://$proxy_x_origin;
            # proxy_intercept_errors on;
            # error_page 302 301 = @handle_redirects;
        }
    }

    server {
        listen 80;
        server_name local-stackdriver-agent.stackdriver.com;
        location /nginx_status {
            stub_status on;
            access_log off;
            allow 127.0.0.1;
        }
        location / {
            root /dev/null;
        }
    }
    server {
        listen 8081;
        server_name ~^(.+)$;
        location / {
            access_log off;
            root /var/www;
        }
    }
}
