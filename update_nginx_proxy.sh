#!/bin/bash

update_nginx_conf() {
    current_nginx_conf_file=$(cat /etc/nginx/current_nginx_conf)
    nginx_conf_gcs=nginx_gcs.conf
    source /mnt/gcs/global_conf

    new_conf_file=$nginx_conf_gcs
    echo "current conf file: $current_nginx_conf_file, conf file in global_conf: $new_conf_file"

    if [ "$current_nginx_conf_file" != "$new_conf_file" ]; then
        echo 'nginx conf file name changed. check if new file exists'
        if [ -e /mnt/gcs/$new_conf_file ]; then
            echo "file $new_conf_file exists, check if valid"
            ln -sf /mnt/gcs/$new_conf_file /etc/nginx/nginx.conf
            check_valid=$(nginx -t 2>&1 | grep "successful")
            if [ -n "$check_valid" ]; then
                echo "file $new_conf_file valid, reload nginx"
                is_running=$(service nginx status | grep running)
                if [ "$is_running" = "" ]; then
                    echo "start nginx service because it is not running"
                    service nginx start
                fi
                service nginx reload
                echo $new_conf_file >/etc/nginx/current_nginx_conf
                gcloud logging write nginx-conf $(hostname)": nginx conf file /mnt/gcs/$new_conf_file reloaded"
            else
                ln -sf /mnt/gcs/$current_nginx_conf_file /etc/nginx/nginx.conf
                echo "file $new_conf_file NOT valid, will NOT reload nginx"
                gcloud logging write nginx-conf $(hostname)": nginx conf file /mnt/gcs/$new_conf_file NOT valid by niginx -t check" --severity=ERROR
            fi
        else
            echo "file /mnt/gcs/$new_conf_file NOT exists, please check key nginx_conf_localssd in /mnt/gcs/global_conf file"
            gcloud logging write nginx-conf $(hostname)": nginx conf file /mnt/gcs/$new_conf_file NOT exists" --severity=ERROR
        fi
    else
        echo 'nginx conf file name not changed'
    fi
}

update_crossdomain_xml() {
    echo 'update crossdomain.xml'
    wget -O /tmp/crossdomain.xml "https://upos-sze6fcab1avod-hkep.bilivideo.com/crossdomain.xml" -q
    if [ -f "/tmp/crossdomain.xml" ]; then
        /snap/bin/gcloud storage cp /tmp/crossdomain.xml gs://xinchen-cdn-crossdomain/ >/dev/null 2>&1
    fi
}

exec 1>>/tmp/nginx_update_$(date -d now +%Y%m%d).log 2>&1

echo "start time"$(date "+%Y-%m-%d %H:%M:%S")"*************************************************"

#bash /mnt/gcs/command.sh

mount_result=$(df -h | grep /mnt/gcs)
if [[ -z $mount_result ]]; then
    echo 'not mounted, waiting for /mnt/gcs initialized by init_nginx_proxy.sh'
    exit
fi

update_nginx_conf
update_crossdomain_xml

echo "end time"$(date "+%Y-%m-%d %H:%M:%S")"***************************************************"

echo ''
